package de.Kiwi.OwnFarmWorld.Commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Transfer_CMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender player, Command cmd, String arg2, String[] args) {

        if (player instanceof Player && ((Player) player).isOp()) {
            if(args.length == 1) {

                String worldName = args[0];
                World w = Bukkit.getWorld(worldName);
                if(w == null) {
                    player.sendMessage("That world doesn't exist!");
                    return false;
                }

                for(int i = 255; i != 0; i--) {
                    Location loc = new Location(w, 0, i, 0);
                    if(loc.getBlock().getType() != Material.AIR) {
                        if(loc.getBlock().getType() == Material.WATER) {
                            loc.getBlock().setType(Material.STONE);
                        }
                        loc.add(0,1,0);
                        ((Entity) player).teleport(loc);
                        return true;
                    }
                }

            }
        }
        return false;
    }
}
