package de.Kiwi.OwnFarmWorld.Commands;

import java.io.File;

import de.Kiwi.OwnFarmWorld.Config.Manager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class Delete_World_CMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command cmd, String arg2, String[] args) {
        if (s instanceof Player) {
            Player p = (Player) s;
            if (p.isOp()) {
                for (Player AP : Bukkit.getOnlinePlayers()) {
                    AP.teleport(Bukkit.getWorld("world").getSpawnLocation());
                }

                World delete = Bukkit.getWorld(Manager.txt("FarmWorldName"));
                Bukkit.unloadWorld(delete, false);
                File deleteFolder = delete.getWorldFolder();

                deleteWorld(deleteFolder);
                p.sendMessage(ChatColor.AQUA + "World has been deleted");
                for (Player AP : Bukkit.getOnlinePlayers()) {
                    if (AP.getWorld().getName().equals(Manager.txt("FarmWorldName"))) {
                    }
                    AP.setPlayerListName(ChatColor.GREEN + AP.getName());
                    if (AP.getPlayer().getGameMode().equals(GameMode.SPECTATOR)
                            || AP.getPlayer().getGameMode().equals(GameMode.SURVIVAL)) {
                        AP.setGameMode(GameMode.ADVENTURE);
                        AP.setHealth(20);
                        AP.setFoodLevel(20);
                        AP.getInventory().clear();
                    }
                }


                Bukkit.getServer().reload();
            }

        }
        return false;
    }

    public boolean deleteWorld(File path) {
        if (path.exists()) {
            File files[] = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteWorld(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }
}