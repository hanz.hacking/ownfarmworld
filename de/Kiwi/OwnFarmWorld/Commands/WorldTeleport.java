package de.Kiwi.OwnFarmWorld.Commands;

import de.Kiwi.OwnFarmWorld.Config.Manager;
import de.Kiwi.OwnFarmWorld.Main.Main;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.xml.sax.Locator;

public class WorldTeleport implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] strings) {
        Player p = (Player) sender;
        if (p.hasPermission(Manager.txt("WorldTeleport"))) {



                Main.state = 1;
                new BukkitRunnable() {
                    int time = 5;

                    @Override
                    public void run() {
                        if (time == 5) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You will be teleported in 5 seconds");
                        }
                        if (time == 4) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You will be teleported in 4 seconds");
                        }
                        if (time == 3) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You will be teleported in 3 seconds");
                        }
                        if (time == 2) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You will be teleported in 2 seconds");
                        }
                        if (time == 1) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You will be teleported in 1 seconds");
                        }
                        if (time == 0) {
                            p.sendMessage(Main.Prefix + ChatColor.GOLD + "You are teleported");


                            World w = Bukkit.getWorld("world");
                            if (w == null) {
                                p.sendMessage(Main.Prefix + "World doesn't exist");
                                return;
                            }

                            for (int i = 255; i != 0; i--) {
                                Location loc = new Location(w, 0, i, 0);
                                if (loc.getBlock().getType() != Material.AIR) {
                                    if (loc.getBlock().getType() == Material.WATER) {
                                        loc.getBlock().setType(Material.STONE);
                                    }
                                    loc.add(0, 1, 0);
                                    ((Player) p).teleport(loc);
                                    return;

                                }
                                this.cancel();
                            }
                        }
                        time--;
                        return;
                    }


                }.runTaskTimer(Main.pl, 0L, 20L);


            }
            return false;
        }
    }

