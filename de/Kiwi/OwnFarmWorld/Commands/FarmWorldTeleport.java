package de.Kiwi.OwnFarmWorld.Commands;

import de.Kiwi.OwnFarmWorld.Config.Manager;
import de.Kiwi.OwnFarmWorld.Main.Main;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.xml.sax.Locator;

public class FarmWorldTeleport implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] strings) {
        Player p = (Player) sender;
        if (p.hasPermission(Manager.txt("WorldTeleport"))) {
            World w = Bukkit.getWorld(Manager.txt("FarmWorldName"));
            if (w == null) {
                p.sendMessage(Main.Prefix + "World doesn't exist");
                return false;
            }
            p.sendTitle(ChatColor.GOLD + Manager.txt("FarmWorldName"),"Have fun");
            for (int i = 255; i != 0; i--) {
                Location loc = new Location(w, 0, i, 0);
                if (loc.getBlock().getType() != Material.AIR) {
                    if (loc.getBlock().getType() == Material.WATER) {
                        loc.getBlock().setType(Material.STONE);
                    }
                    loc.add(0, 1, 0);
                    ((Player) p).teleport(loc);
                    return true;

                }
            }
        }
        return false;
    }
}

