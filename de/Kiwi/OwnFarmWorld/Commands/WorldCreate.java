package de.Kiwi.OwnFarmWorld.Commands;

import de.Kiwi.OwnFarmWorld.Config.Manager;
import de.Kiwi.OwnFarmWorld.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WorldCreate implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] strings) {
        Player p =(Player) sender;
        if (p.hasPermission(Manager.txt("WorldCreate")) || p.isOp()) {
            //if(Bukkit.getWorld(Manager.txt("FarmWorldName")) == null) {
            p.sendMessage(Main.Prefix + "Please be patient this may take some time please do not move");
                WorldCreator wc = new WorldCreator(Manager.txt("FarmWorldName"));
                wc.type(WorldType.NORMAL);

                wc.createWorld();

                Bukkit.getServer().broadcastMessage("World has been Created");
          //  }
        } else {
            p.sendMessage(Main.Prefix + Main.MessageNotPerm);
        }
        return false;
    }
}
