package de.Kiwi.OwnFarmWorld.Config;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;

public class Manager {
        public static YamlConfiguration cfg = YamlConfiguration.loadConfiguration(Create.config);



        public static void ConfigFile() {
                cfg.options().header("OwnFarmWorld Config | By KiwiLetsPlay");
                cfg.set("Settings","Settings\n");
                cfg.set("FarmWorldName", "FarmWorld");

                cfg.set("Messages","Settings\n");
                cfg.set("DontHavePerm", "You don't have Permissions");

                cfg.set("Permissions","Settings\n");
                cfg.set("WorldCreate","farmworld.create");
                cfg.set("WorldTeleport","farmworld.teleport");


                save(cfg);
        }

        private static void save(YamlConfiguration cfg) {
                try {
                        cfg.save(Create.config);
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }

        public static String col(String key) {
                String s = (String) cfg.get(key);
                return ChatColor.translateAlternateColorCodes('&', s);
        }

        public static String txt(String key) {

                return (String) cfg.getString(key);
        }
}
