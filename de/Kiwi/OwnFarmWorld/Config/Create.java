package de.Kiwi.OwnFarmWorld.Config;

import java.io.File;
import java.io.IOException;

public class Create {
    public static File dir = new File("plugins/OwnFarmWorld");
    public static File config = new File(dir.getPath() + "/Config.yml");


    public static void Create(){
        if (!dir.exists()) {
            dir.mkdir();

        }

        if (!config.exists()){
            try {
                config.createNewFile();
                Manager.ConfigFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
