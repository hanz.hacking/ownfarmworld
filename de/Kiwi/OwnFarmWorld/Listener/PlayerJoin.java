package de.Kiwi.OwnFarmWorld.Listener;

import de.Kiwi.OwnFarmWorld.Config.Manager;
import de.Kiwi.OwnFarmWorld.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        World w = Bukkit.getWorld(Manager.txt("FarmWorldName"));
        if (w == null) {
            p.sendMessage(Main.Prefix + ChatColor.RED +  Manager.txt("FarmWorldName") + ChatColor.WHITE + " Wolrd doesn't exist");
        }
    }
}
