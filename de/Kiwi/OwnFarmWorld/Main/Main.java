package de.Kiwi.OwnFarmWorld.Main;

import de.Kiwi.OwnFarmWorld.Commands.*;
import de.Kiwi.OwnFarmWorld.Config.Create;
import de.Kiwi.OwnFarmWorld.Config.Manager;
import de.Kiwi.OwnFarmWorld.Listener.PlayerJoin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    public static Plugin pl;
    public static String Prefix = "[" + ChatColor.AQUA + "OwnFarmWorld" + ChatColor.WHITE + "] ";
    public static String WorldCreate = Manager.txt("WorldCreate");
    public static String WorldTeleport = Manager.txt("WorldTeleport");
    public static String MessageNotPerm = Manager.txt("DontHavePerm");
    public static int state = 0;

    public void onEnable() {
        pl = this;
        Create.Create();
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getCommand("farmworldcreate").setExecutor(new WorldCreate());
        getCommand("farmworld").setExecutor(new FarmWorldTeleport());
        getCommand("world").setExecutor(new WorldTeleport());
        getCommand("transfer").setExecutor(new Transfer_CMD());
        getCommand("deletefarmworld").setExecutor(new Delete_World_CMD());

        World w = Bukkit.getWorld(Manager.txt("FarmWorldName"));
            if (w == null) {
                Bukkit.getServer().broadcastMessage(Main.Prefix + "FarmWorld is not yet created");

            }
        }
    }


